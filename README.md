# login-ssh
saves public key on ssh and creates db entry

## Add server
```
login-ssh -a <server-name> <server-url>
```

## View servers
```
login-ssh
```

## Connect to server
```
login-ssh <server-name>
```
