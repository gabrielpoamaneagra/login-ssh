'use strict';

var fs          = require('fs');
var spawn       = require('child_process').spawn;
var sprintf     = require('sprintf-js').sprintf;

var args        = process.argv.slice(2);
var scriptName  = 'login-ssh';
var servers     = JSON.parse(fs.readFileSync(__dirname + '/servers.json', 'utf8'));;

var exit = function() {
    process.exit(0);
};

if (args.length == 0) {
    console.log('\nTo add a server use "' + scriptName + ' -a name url"');
    console.log(new Array(50).join('-'));
    console.log('\n');

    if (servers.length) {
        console.log('Registered servers: ');

        servers.forEach(function(server) {
            console.log(sprintf('%15s     %s', server.name, server.url));
        });
    }

    console.log('To connect use "' + scriptName + ' name"\n');
    exit();
}

var name = '';

if (args[0] == '-a') {
    if (args.length != 3) {
        console.log('Invalid argument count for command "' + scriptName + ' -a name url"');
        exit();
    }

    name        = args[1];
    var url         = args[2];

    servers.push({
        name    : name,
        url     : url
    });

    fs.writeFile(__dirname + '/servers.json', JSON.stringify(servers), 'utf-8');

    spawn('sh', ['-c', 'ssh-copy-id ' + url], {
        stdio: 'inherit'
    });
} else {
    if (args.length != 1) {
        console.log('Invalid argument count for command "' + scriptName + ' name"');
        exit();
    }
    name = args[0];

    var server = null;

    servers.forEach(function(s) {
        if (s.name == name) {
            server = s;

            return false;
        }
    });

    if (!server) {
        console.log('Server ' + name + ' not found');
        exit();
    }

    spawn('sh', ['-c', 'ssh ' + server.url], {
        stdio: 'inherit'
    });
}
